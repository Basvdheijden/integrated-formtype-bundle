# IntegratedFormTypeBundle #
The IntegratedFormTypeBundle provides different Symfony Form Types for the Integrated project.

## Documentation ##
The documentation is stored in the `Resources/doc/index.md`.

[Read the Documentation](Resources/doc/index.md)

## Installation ##
The installation instructions can be found in the documentation.

## About ##
The IntegratedFormTypeBundle is part of the Intergrated project.